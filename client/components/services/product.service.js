'use strict';

    function Service(SessionService, APIService) {
        this.SessionService = SessionService;
        this.APIService = APIService;
    }

    Service.prototype = {
        createProduct: function(transferObject) {
            transferObject.userId = this.SessionService.getUserData().id;
            return this.APIService.createData('products', transferObject);
        },
        getProducts: function() {
            return this.APIService.getData('products');
        }
    };

    angular.module('cpApp')
      .service('ProductService', Service);
