'use strict';

    function Service(SessionService, APIService) {
        this.SessionService = SessionService;
        this.APIService = APIService;
    }

    Service.prototype = {
        createPedido: function(transferObject) {
            transferObject.comercioId = this.SessionService.getUserData().entityData.id;
            return this.APIService.createData('pedidos_comerciantes', transferObject);
        },

        getPedidosByComercianteId: function(comercianteId) {
            return this.APIService.getData('pedidos_comerciantes', comercianteId);
        },

        getPedidosByProveedorId: function(proveedorId) {
            return this.APIService.getData('pedidos_proveedores', proveedorId);
        },

        getPedidoById: function(pedidoId) {
            return this.APIService.getData('pedidos', pedidoId);
        }
    };

    angular.module('cpApp')
      .service('PedidosService', Service);
