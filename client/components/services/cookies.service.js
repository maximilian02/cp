'use strict';

    function Service($rootScope, $cookies, $cookieStore, $location, $window) {
        this.$window = $window;
        this.$rootScope = $rootScope;
        this.$cookies = $cookies;
        this.$location = $location;

        this.initializeCookie();
    }

    Service.prototype = {
        initializeCookie: function() {
            var currentCookie = this.$cookies.getObject('sessionCookie');
            if(!currentCookie) {
                // console.log('NO COOKIE, LETS CREATE ONE!!!!');
                this.$cookies.putObject('sessionCookie', {
                  logged: false,
                  userData: null
                });
            } else {
                // console.log('COOKIE ALREADY CREATED!!!!');
            }
        },

        getKey: function(key) {
            var cookieObject = this.$cookies.getObject('sessionCookie');
            return cookieObject[key];
        },

        getCookie: function() {
            return this.$cookies.getObject('sessionCookie');
        },

        broadCastRefreshUserData: function() {
            // firing event when user data is updated
            this.$rootScope.$broadcast('refreshUserData');
        },

        executeLogIn: function(userData) {
            // Getting current cookie
            var currentCookie = this.$cookies.getObject('sessionCookie');
            // Setting new values
            currentCookie.logged = true;
            currentCookie.userData = userData;
            // Saving cookie
            this.$cookies.putObject('sessionCookie', currentCookie);
            //Broadcast Refresh data request
            this.broadCastRefreshUserData();
        },

        executeLogOut: function(userData) {
            // Getting current cookie
            var currentCookie = this.$cookies.getObject('sessionCookie');
            // Setting new values
            currentCookie.logged = false;
            currentCookie.userData = null;
            // Saving cookie
            this.$cookies.putObject('sessionCookie', currentCookie);
            // Broadcast to refresh data
            this.broadCastRefreshUserData();
            // Redirect to home
            this.$location.path('/logout');
        },

        isLogged: function() {
            return this.getKey('logged');
        },

        getUserData: function() {
            return this.getKey('userData');
        },

        redirectIfLogged: function(redirection) {
            if(this.isLogged()) {
                this.$location.path(redirection);
            }
        },

        redirectIfNotLogged: function(redirection) {
            if(!this.isLogged()) {
                this.$location.path(redirection);
            }
        },

        updateCookies: function(userData) {
            // Getting current cookie
            var currentCookie = this.$cookies.getObject('sessionCookie');
            // Setting new values
            currentCookie.userData = userData;
            // Saving cookie
            this.$cookies.putObject('sessionCookie', currentCookie);
            //Broadcast Refresh data request
            this.broadCastRefreshUserData();
        },

        verifyType: function(type) {
            this.redirectIfNotLogged('/');
            if (type !== this.getKey('userData').userType) {
                this.$location.path('/');
            }
        },

        verifyEntity: function() {
            this.redirectIfNotLogged('/');
            if(!this.getKey('userData').entityData) {
                this.$location.path('/');
            }
        },
    };

    angular.module('cpApp')
      .service('SessionService', Service);
