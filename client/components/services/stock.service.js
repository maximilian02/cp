'use strict';

    function Service(SessionService, APIService, HelperService) {
        this.APIService = APIService;
        this.HelperService = HelperService;
        this.userData = SessionService.getUserData();
    }

    Service.prototype = {
        getStock: function() {
            return this.APIService.getData('stock');
        },

        getMovimientos: function() {
            return this.APIService.getData('movimientos', this.userData.entityData.id, this.userData.userType);
        },

        partitionMovimientos: function(data){
            var keys = Object.keys(data),
                movimientos =  { 1: [], 2: [] };

            for (var i = 0; i < keys.length; i++) {
                var movimiento = data[keys[i]];
                movimientos[movimiento.Tipo].push(movimiento);
            }

            return this.HelperService.transformToArray(movimientos);
        },

        calculateProductsAvailable: function(data) {
            var movimientos, keys,
                ingresos = [], egresos = [],
                productsObject = {},
                productsArray = [],
                movimientos;

            if(data) {
                movimientos = this.partitionMovimientos(data.movimientos);

                ingresos = movimientos[0] ? movimientos[0] : [],
                egresos = movimientos[1] ? movimientos[1] : [];

                console.log('movimientos', movimientos);
                console.log(ingresos);

                //Ingresos
                for (var i = 0; i < ingresos.length; i++) {
                    var ingreso = ingresos[i],
                        product = ingreso.Producto;
                        product.Cantidad = ingreso.Cantidad;

                    if(productsObject.hasOwnProperty(product.Id)) {
                        productsObject[product.Id].Cantidad += product.Cantidad;
                    } else {
                        productsObject[product.Id] = product;
                    }
                }

                // Egresos
                for (var i = 0; i < egresos.length; i++) {
                    var egreso = egresos[i],
                        product = egreso.Producto;
                        product.Cantidad = egreso.Cantidad;

                    if(productsObject.hasOwnProperty(product.Id)) {
                        productsObject[product.Id].Cantidad -= product.Cantidad;
                    } else {
                        productsObject[product.Id] = product;
                    }

                    if(productsObject[product.Id].Cantidad <= 0) {
                        delete productsObject[product.Id];
                    }
                }

                productsArray = this.HelperService.transformToArray(productsObject);

                return { productArray: productsArray, productObject: productsObject };
            }

            return [];
        },

        createIngreso: function(data) {
            data.userData = {
                userType: this.userData.userType,
                entityId: this.userData.entityData.id
            };
            return this.APIService.createData('ingresos', data);
        },

        createEgreso: function(data) {
            data.userData = {
                userType: this.userData.userType,
                entityId: this.userData.entityData.id
            };
            return this.APIService.createData('egresos', data);
        }
    };

    angular.module('cpApp')
      .service('StockService', Service);
