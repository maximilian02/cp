'use strict';

    function Service(SessionService, APIService) {
        this.SessionService = SessionService;
        this.APIService = APIService;
        this.userData = this.SessionService.getUserData();
    }

    Service.prototype = {
        // Deletes
        deleteLista: function(listaId) {
            return this.APIService.deleteData('listas', listaId);
        },
        deletePrecio: function(precioId) {
            return this.APIService.deleteData('precios', precioId);
        },

        // Updates
        updateLista: function(listaId, transferObject) {
            return this.APIService.updateData('listas', listaId, transferObject);
        },

        // Creates
        createLista: function(transferObject) {
            transferObject.entityId = this.userData.entityData.id;
            return this.APIService.createData('listas', transferObject);
        },
        createPrecio: function(transferObject) {
            return this.APIService.createData('precios', transferObject);
        },

        // Getters
        getPreciosByListaId: function(listaId) {
            return this.APIService.getData('precios', listaId);
        },
        getListas: function(noAuth) {
            if(noAuth) {
                return this.APIService.getData('listas');
            } else {
                this.userData = this.SessionService.getUserData();
                return this.APIService.getData('listas', this.userData.entityData.id);
            }
        },
        getLista: function(listaId) {
            return this.APIService.getData('listas', this.userData.entityData.id, listaId);
        },

        getListaOffline: function(listaId) {
            return this.APIService.getData('listasoff', listaId);
        },

        getListasByProveedorIdOffline: function(listaId) {
            return this.APIService.getData('listas', listaId);
        }
    };

    angular.module('cpApp')
      .service('PreciosService', Service);
