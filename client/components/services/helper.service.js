'use strict';

    function Service() {}

    Service.prototype = {
        validarCuit: function(cuit) {
          var mult = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2],
              total = 0,
              mod, digito;

          if (typeof (cuit) == 'undefined') {
              return true;
          }
          cuit = cuit.toString().replace(/[-_]/g, "");

          if (cuit == '') {
              return true; //No estamos validando si el campo esta vacio, eso queda para el "required"
          }
          if (cuit.length != 11) {
              return false;
          } else {
              for (var i = 0; i < mult.length; i++) {
                  total += parseInt(cuit[i]) * mult[i];
              }

              mod = total % 11;
              digito = mod == 0 ? 0 : mod == 1 ? 9 : 11 - mod;
          }

           return digito == parseInt(cuit[10]);
        },

        validarTelefono: function(numeroTelefono) {
            var regex = RegExp(/^[\+]?[(]?[0-9]{1,}[)]?[-\s\.]?[0-9]{1,}[-\s\.]?[0-9]{4,6}$/im);
            return regex.test(numeroTelefono);
        },

        getPreferenciaPagoById: function (preferenciaId) {
            var idPreferencia = {
              1: 'Efectivo',
              2: 'Deposito / Transferencia',
              3: 'Cheque',
              4: 'Otro'
            };

            return idPreferencia[preferenciaId] ? idPreferencia[preferenciaId] : '';
        },

        getEstadoPedidoById: function (estadoId) {
            var idEstado = {
              0: 'Rechazado',
              1: 'Entregado',
              2: 'En proceso'
            };

            return idEstado[estadoId] ? idEstado[estadoId] : '';
        },

        transformToArray: function(productsObject) {
            var arr = Object.keys(productsObject).map(function (key) {
              return productsObject[key];
            });
            return arr;
        },

        formatDate: function(dateString) {
            var formatDate = '';
            if(dateString) {
                formatDate = new Date(dateString);
                formatDate = formatDate.getDate() + '/' + (formatDate.getMonth() + 1) + '/' + formatDate.getFullYear();
            }

            return formatDate;
        },

        formatDateTime: function(dateString) {
            var formatDate = '';
            if(dateString) {
                formatDate = new Date(dateString);
                formatDate = formatDate.getDate() + '/' + (formatDate.getMonth() + 1) + '/' + formatDate.getFullYear() + ' ' + formatDate.getHours() + ':' + formatDate.getMinutes();
            }

            return formatDate;
        }
    };

    angular.module('cpApp')
      .service('HelperService', Service);
