'use strict';

    function Service($http, $resource) {
        // Setting headers
        $http.defaults.headers.common['Content-Type'] = 'application/json',
        $http.defaults.headers.common['apiKey'] = NG_APP_ENV.key,
        this.$resource = $resource;
        this.baseUrl = NG_APP_ENV.api + ':resourceName/:resourceId/:subResourceId',
        this.resource = {},

        this.paramDefaults = {
          resourceName: "@resourceName",
          subResourceId: "@subResourceId",
          resourceId: '@resourceId'
        },

        this.actions = {
            get:    {method: 'GET', isArray: false, withCredentials: false},
            post:   {method: 'POST', isArray: false, withCredentials: false},
            put:    {method: 'PUT', isArray: false, withCredentials: false},
            delete: {method: 'DELETE', isArray: false, withCredentials: false}
        };
    }

    Service.prototype = {
        prepareQuery: function(entity, dataId, subDataId) {
            var query = {
                resourceName: entity
            }

            if(dataId) {
                query.resourceId = dataId;
            }

            if(subDataId) {
                query.subResourceId = subDataId;
            }

            return query;
        },

        executeRequest: function(method, query, data) {
            this.resource = this.$resource(this.baseUrl, this.paramDefaults, this.actions);
            return this.resource[method](query, data);
        },

        getData: function (entity, dataId, subDataId) {
            return this.executeRequest('get', this.prepareQuery(entity, dataId, subDataId));
        },

        createData: function (entity, data) {
            return this.executeRequest('post', this.prepareQuery(entity), data);
        },

        updateData: function (entity, dataId, data) {
            return this.executeRequest('put', this.prepareQuery(entity, dataId), data);
        },

        deleteData: function (entity, dataId) {
            return this.executeRequest('delete', this.prepareQuery(entity, dataId));
        },

        login: function (data) {
            return this.executeRequest('post', this.prepareQuery('login'), data);
        }
    };

    angular.module('cpApp')
      .service('APIService', Service);
