'use strict';

    function Service(SessionService, APIService) {
        this.SessionService = SessionService;
        this.APIService = APIService;
    }

    Service.prototype = {
        getProveedor: function(proveedorId) {
            return this.APIService.getData('proveedores', proveedorId);
        },

        createProveedor: function(transferObject) {
            transferObject.userId = this.SessionService.getUserData().id;
            return this.APIService.createData('proveedores', transferObject);
        },

        updateProveedor: function(transferObject) {
            var entityId = this.SessionService.getUserData().entityData.id;
            return this.APIService.updateData('proveedores', entityId, transferObject);
        }
    };

    angular.module('cpApp')
      .service('ProveedorService', Service);
