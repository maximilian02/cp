'use strict';

    function Service(APIService, SessionService) {
        this.SessionService = SessionService;
        this.APIService = APIService;
        this.userData = this.SessionService.getUserData();

        var stock = {
              sectionName: 'Stock',
              sectionDescription: 'Aquí podrá gestionar su stock. Generando ingresos cuando usted compra mercadería, o generando egresos cuando usted realiza una venta.',
              needWarnig: true,
              warningMessage: function(userType) {
                  return  'Por favor aseguresé de ingresar los datos del ' + (userType === 1 ? 'comercio' : 'proveedor') +' para poder utilizar las acciones sobre el Stock.';
              },
              validate: true,
              actions: [
                  {
                    sectionName: "Mi Stock",
                    sectionPath: "/stock",
                    sectionDescription: "Mostrar mi stock actual.",
                    isBeta: true
                  },
                  {
                    sectionName: "Generar ingreso",
                    sectionPath: "/ingreso",
                    sectionDescription: "Generar un ingreso de mercadería.",
                    isBeta: true
                  },
                  {
                    sectionName: "Generar egreso",
                    sectionPath: "/egreso",
                    sectionDescription: "Generar un egreso de mercadería. Puede ser utilizado para representar una venta.",
                    isBeta: true
                  }
              ]
            },
            productos = {
              sectionName: 'Productos',
              sectionDescription: 'Aquí podrá crear un nuevo producto en el caso de que no lo encuentre en nuestra base de datos.',
              validate: false,
              actions: [
                  {
                    sectionName: "Crear producto",
                    sectionPath: "/alta-producto",
                    sectionDescription: "Crear un producto.",
                    isBeta: true
                  },
                  {
                    sectionName: "Listar productos",
                    sectionPath: "/productos",
                    sectionDescription: "Listar los productos existentes.",
                    isBeta: true
                  }
              ]
            },
            reportes = {
              sectionName: 'Reportes',
              sectionDescription: 'Aquí podrá ver los reportes de egresos e ingresos realizados.',
              validate: false,
              actions: [
                  {
                    sectionName: "Reporte general",
                    sectionPath: "/reportes",
                    sectionDescription: "Aquí podrá ver un reporte general de los ingresos y egresos realizados.",
                    isBeta: true
                  }
              ]
            },
            listas = {
              sectionName: 'Listas de Precios',
              sectionDescription: 'Aquí podrá gestionar las listas de precios.',
              needWarnig: true,
              warningMessage: function(userType) {
                  return  'Por favor aseguresé de ingresar los datos del proveedor para poder utilizar las listas de precios.';
              },
              validate: true,
              actions: [
                  {
                    sectionName: "Ver listas de precios",
                    sectionPath: "/listas",
                    sectionDescription: "Visualizar las listas existentes.",
                    isBeta: true
                  },
                  {
                    sectionName: "Crear lista de precios",
                    sectionPath: "/alta-lista",
                    sectionDescription: "Crear una nueva lista de precios.",
                    isBeta: true
                  }
              ]
            },
            pedidosComerciante = {
              sectionName: 'Pedidos',
              sectionDescription: 'Aquí podrá ver y realizar pedidos.',
              needWarnig: true,
              warningMessage: function(userType) {
                  return  'Por favor aseguresé de ingresar los datos del comercio para poder realizar un pedidos.';
              },
              validate: true,
              actions: [
                  {
                    sectionName: "Ver pedidos realizados",
                    sectionPath: "/pedidos-comerciante",
                    sectionDescription: "Ver pedidos realizados.",
                    isBeta: true
                  }
                  // },
                  // {
                  //   sectionName: "Crear pedidos",
                  //   sectionPath: "/alta-pedido",
                  //   sectionDescription: "Crear un nuevo pedido.",
                  //   isBeta: true
                  // }
              ]
            },
            pedidosProveedor = {
              sectionName: 'Pedidos',
              sectionDescription: 'Aquí podrá ver los pedidos entrantes.',
              needWarnig: true,
              warningMessage: function(userType) {
                  return  'Por favor aseguresé de ingresar los datos del proveedor para poder recibir pedidos.';
              },
              validate: true,
              actions: [
                  {
                    sectionName: "Ver pedidos",
                    sectionPath: "/pedidos-proveedor",
                    sectionDescription: "Visualizar los pedidos entrantes.",
                    isBeta: true
                  }
              ]
            };
            // precios = {
            //   sectionName: 'Precios',
            //   sectionPath: '/precios',
            //   sectionDescription: 'Aquí podrá gestionar los precios.',
            //   validate: false,
            //   actions: [
            //       {
            //         sectionName: "Listar precios",
            //         sectionPath: "/precios",
            //         sectionDescription: "Visualizar las listas existentes.",
            //         isBeta: true
            //       },
            //       {
            //         sectionName: "Crear precio",
            //         sectionPath: "/alta-precio",
            //         sectionDescription: "Crear una nueva lista de precios.",
            //         isBeta: true
            //       },
            //       {
            //         sectionName: "Modificar precio",
            //         sectionPath: "/modificar-precio/",
            //         sectionDescription: "Modificar una nueva lista de precios.",
            //         isBeta: true
            //       }
            //   ]
            // };

        this.userTypeAllowedSection = {
            // Tipo Usuario Id 1: Comerciantes
            1: [productos, stock, pedidosComerciante, reportes],
            // Tipo Usuario Id 2: Proveedor
            2: [productos, stock, listas, pedidosProveedor, reportes]
        }
    }

    Service.prototype = {
        getPanelDataByUserTypeId: function(userTypeId) {
            return this.userTypeAllowedSection[userTypeId];
        },

        updateUserData: function (userId) {
            this.APIService.getData('login', userId).$promise.then(
              function (res) {
                this.SessionService.updateCookies(res.data.userData);
              }.bind(this)
            );
        },

        getUserData: function() {
            return this.userData;
        }
    };

    angular.module('cpApp')
      .service('UserService', Service);
