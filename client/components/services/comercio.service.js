'use strict';

    function Service(SessionService, APIService) {
        this.SessionService = SessionService;
        this.APIService = APIService;
    }

    Service.prototype = {
        createComercio: function(transferObject) {
            transferObject.userId = this.SessionService.getUserData().id;
            return this.APIService.createData('comercios', transferObject);
        },

        updateComercio: function(transferObject) {
            var entityId = this.SessionService.getUserData().entityData.id;
            return this.APIService.updateData('comercios', entityId, transferObject);
        }
    };

    angular.module('cpApp')
      .service('ComercioService', Service);
