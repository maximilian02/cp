'use strict';

    function Controller($rootScope, $scope, $location, SessionService, UserService) {
        $scope.menu = [
          // {
          //   'title': 'Productos',
          //   'link': '/products'
          // },
          // {
          //   'title': 'Proveedores',
          //   'link': '/proveedores'
          // },
          // {
          //   'title': 'Listas',
          //   'link': '/listas'
          // },
          // {
          //   'title': 'Kioscos',
          //   'link': '/kioscos'
          // }
        ];

        $scope.isLogged = SessionService.isLogged();
        $scope.userData = UserService.getUserData();
        $scope.isCollapsed = true;

        $scope.logout = function() {
            SessionService.executeLogOut();
        };

        $scope.isActive = function(route) {
          var current = $location.path();
          return current.indexOf(route) !== -1;
        };

        // listen for the refreshUserData event
        $rootScope.$on('refreshUserData', function (event, data) {
            $scope.isLogged = SessionService.isLogged();
            $scope.userData = SessionService.getUserData();
        });

    }

angular.module('cpApp')
  .controller('NavbarController', Controller);
