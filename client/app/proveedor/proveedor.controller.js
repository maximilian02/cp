'use strict';

angular.module('cpApp')
  .controller('ProveedorController', function ($rootScope, $scope, $location, $routeParams, ProveedorService, HelperService, PreciosService) {
      $scope.dataValues = {};
      $scope.proveedorData = {};
      $scope.proveedorData.id = $routeParams.proveedorId;
      $scope.listasArray = [];

      $scope.getProveedor = function() {
          ProveedorService.getProveedor($scope.proveedorData.id).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.proveedorData = HelperService.transformToArray(res.data.proveedor)[0];
                  }
              }
          );
      };

      $scope.getListas = function() {
          PreciosService.getListasByProveedorIdOffline($scope.proveedorData.id).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.listasArray = HelperService.transformToArray(res.data.listas);
                      console.log($scope.listasArray);
                  }
              }
          );
      };

      $scope.formatDate = function(dateString) {
          return HelperService.formatDate(dateString);
      }

      $scope.getPreferenciaPago = function(preferenciaId) {
          return HelperService.getPreferenciaPagoById(preferenciaId);
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if(current.indexOf('proveedor') !== -1) {
              $scope.getProveedor();
              $scope.getListas();
          }
      };

      $scope.detectMode();
  });
