'use strict';

angular.module('cpApp')
  .controller('ProveedoresController', function ($rootScope, $scope, $location, ProveedorService, UserService, HelperService, SessionService, $window) {
      $scope.actionSuccess = false;
      $scope.actionError = false;
      $scope.userData = SessionService.getUserData();
      $scope.userType = $scope.userData.userType;
      $scope.telefonoHasChange = false;
      $scope.dataValues = {};

      $scope.createProveedor = function (data) {
          $scope.actionSuccess = true;
          ProveedorService.createProveedor(data).$promise.then(
            function (res) {
              if(res.data.response) {
                  UserService.updateUserData($scope.userData.id);
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };

      $scope.updateProveedor = function (data) {
          $scope.actionSuccess = true;
          ProveedorService.updateProveedor(data).$promise.then(
            function (res) {
              if(res.data.response) {
                  console.log(res.data.response);
                  UserService.updateUserData($scope.userData.id);
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };

      $scope.validarTelefono = function (telefono) {
          return HelperService.validarTelefono(telefono);
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if($scope.userType !== 2) {
              $location.path('/');
          }

          if(current.indexOf('modificar') !== -1) {
              if(!$scope.userData.entityData) {
                  $location.path('/');
              }

              if($scope.userType === 2) {
                  $scope.dataValues = {
                      proveedorEmail: $scope.userData.entityData.email,
                      proveedorContactName: $scope.userData.entityData.nombreContacto,
                      proveedorPreferenciaPago: $scope.userData.entityData.preferenciaPago,
                      proveedorPedidoMinimo: $scope.userData.entityData.pedidoMinimo,
                      proveedorTelefono: $scope.userData.entityData.telefono
                  };
              }
          } else {
              if($scope.userData.entityData) {
                  $location.path('/');
              }
          }
      };

      $scope.detectMode();

      // listen for the refreshUserData event
      $rootScope.$on('refreshUserData', function (event, data) {
          $scope.userData = SessionService.getUserData();
      });
  });
