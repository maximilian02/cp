'use strict';

angular.module('cpApp')
  .controller('PerfilController', function ($scope, HelperService, SessionService) {
      $scope.userData = SessionService.getUserData();
      $scope.userData.userTypeString = $scope.userData.userType === 1 ? 'Comerciante' : 'Proveedor';

      $scope.proccessDataForView = function (item) {
          if(item.needProcess) {
              if(item.fieldName === 'Preferencia de pago') {
                  return HelperService.getPreferenciaPagoById(item.fieldValue);
              }
          } else {
              return item.fieldValue;
          }
      };

      
  });
