'use strict';

angular.module('cpApp')
  .controller('StockController', function ($rootScope, $scope, $location, UserService, ProductService, StockService, SessionService, $route) {
      $scope.productsArray = [],
      $scope.productSelected = false;
      $scope.productObjSelected = {};
      $scope.userData = SessionService.getUserData();
      $scope.hasEntity = $scope.userData.entityData ? true : false;
      $scope.ingresos = {};
      $scope.loading = true;

      $scope.createIngreso = function(data) {
          delete data.productName;
          data.productId = $scope.productObjSelected.Id;

          StockService.createIngreso(data).$promise.then(
              function (res) {
                if(res.data.response) {
                    $scope.actionSuccess = true;
                    $scope.actionError = false;
                } else {
                    $scope.actionSuccess = false;
                    $scope.actionError = true;
                }
              }
          );
      };

      $scope.createEgreso = function(data) {
          delete data.productName;
          data.productId = $scope.productObjSelected.Id;

          StockService.createEgreso(data).$promise.then(
              function (res) {
                if(res.data.response) {
                    $scope.actionSuccess = true;
                    $scope.actionError = false;
                } else {
                    $scope.actionSuccess = false;
                    $scope.actionError = true;
                }
              }
          );
      };

      $scope.getProducts = function() {
          ProductService.getProducts().$promise.then(
              function (res) {
                  $scope.productsArray = res.data.products;
              }
          );
      };

      $scope.getProductsAvailable = function() {
          StockService.getMovimientos().$promise.then(
              function (res) {
                  console.log(res.data);
                  var result = StockService.calculateProductsAvailable(res.data);
                  $scope.productsArray = result.productArray;
                  $scope.productsObject = result.productObject;
                  $scope.loading = false;
                  console.log($scope.productsArray);
              }
          );
      };

      $scope.selectProduct = function(product) {
          $scope.productSelected = true;
          $scope.productObjSelected = product;
      };

      $scope.deleteSelected = function () {
          $scope.productSelected = false;
          $scope.productObjSelected = {};
          $scope.dataValues.productName.$ = '';

          if($scope.dataValues.egresoProductQty) {
            $scope.dataValues.egresoProductQty = '';
          }

          if($scope.dataValues.ingresoProductQty) {
            $scope.dataValues.ingresoProductQty = '';
          }

          if($scope.dataValues.ingresoProductPrice) {
            $scope.dataValues.ingresoProductPrice = '';
          }
      };

      $scope.reloadPage = function () {
          $route.reload();
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if(current.indexOf('ingreso') !== -1) {
              $scope.getProducts();
          }
          if(current.indexOf('egreso') !== -1 || current.indexOf('stock') !== -1) {
              $scope.getProductsAvailable();
          }
      };

      // listen for the refreshUserData event
      $rootScope.$on('refreshUserData', function (event, data) {
          $scope.userData = SessionService.getUserData();
      });

      $scope.detectMode();
  });
