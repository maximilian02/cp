'use strict';

angular.module('cpApp')
  .controller('ComercioPedidoController', function ($routeParams, $scope, $location, HelperService, PreciosService, SessionService, ProductService, ProveedorService, PedidosService) {
      $scope.loading = true;
      $scope.preciosArray = [];
      $scope.pedidosArray = [];
      $scope.listaPreciosData = {};
      $scope.proveedorData = {};
      $scope.dataListaValues = {};
      $scope.actionSuccess = false;
      $scope.actionError = false;
      $scope.loading = true;
      $scope.pedidoActual = {
          preciosObj: {},
          preciosArray: [],
          pedidoComment: ''
      };

      $scope.getPrecios = function() {
          PreciosService.getPreciosByListaId($routeParams.listaId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.listaPreciosData = HelperService.transformToArray(res.data.listaPrecios)[0];
                      $scope.loading = false;
                      $scope.preciosArray = HelperService.transformToArray(res.data.precios);
                      $scope.proveedorData.id = HelperService.transformToArray($scope.listaPreciosData.RProveedorListaprecioss)[0].ProveedorId;
                      $scope.getProveedor($scope.proveedorData.id);
                  }
              }
          );
      };

      $scope.removeFromPedido = function(precio) {
          delete $scope.pedidoActual.preciosObj[precio.PrecioId];
          $scope.pedidoActual.preciosArray = HelperService.transformToArray($scope.pedidoActual.preciosObj);
          $scope.pedidoActual.total = $scope.procesarSuma();
      };

      $scope.createPedido = function() {
          var confirmar = confirm('Está seguro que desea enviar el pedido? Una vez enviado no podrá ser modificado.');
          if(confirmar) {
              $scope.pedidoActual.preciosArray = HelperService.transformToArray($scope.pedidoActual.preciosObj);
              delete $scope.pedidoActual.preciosObj;
              $scope.pedidoActual.proveedorId = $scope.proveedorData.Id;
              PedidosService.createPedido($scope.pedidoActual).$promise.then(
                  function (res) {
                      if(res.data.response) {
                          $scope.actionSuccess = true;
                          console.log($scope.actionSuccess);
                          $scope.actionError = false;
                      } else {
                          $scope.actionSuccess = false;
                          $scope.actionError = true;
                      }
                  }
              );
          }
      };

      $scope.addPrecio = function(precio) {
          precio.Cantidad = $scope.dataListaValues[precio.PrecioId].Cantidad ? $scope.dataListaValues[precio.PrecioId].Cantidad : 0;
          var newPrecio = JSON.parse(JSON.stringify(precio));

          if($scope.pedidoActual.preciosObj[precio.PrecioId]) {
              $scope.pedidoActual.preciosObj[precio.PrecioId].total += newPrecio.Cantidad * newPrecio.Precio.Precio;
              $scope.pedidoActual.preciosObj[precio.PrecioId].Cantidad += newPrecio.Cantidad;
          } else {
              newPrecio.total = newPrecio.Cantidad * newPrecio.Precio.Precio;
              $scope.pedidoActual.preciosObj[precio.PrecioId] = newPrecio;
          }
          $scope.pedidoActual.preciosArray = HelperService.transformToArray($scope.pedidoActual.preciosObj);
          $scope.pedidoActual.total = $scope.procesarSuma();
          $scope.dataListaValues[precio.PrecioId].Cantidad = '';
      };

      $scope.procesarSuma = function() {
          var total = $scope.pedidoActual.preciosArray.reduce(function(prev, curr) {
            return {total: (prev.total + curr.total)};
          }, {total: 0});
          return total.total;
      };

      $scope.getProveedor = function(proveedorId) {
          ProveedorService.getProveedor(proveedorId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.proveedorData = HelperService.transformToArray(res.data.proveedor)[0];
                      // console.log($scope.proveedorData);
                  }
              }
          );
      };

      $scope.getPedidos = function() {
          PedidosService.getPedidosByComercianteId($scope.userData.entityData.id).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.pedidosArray = HelperService.transformToArray(res.data.pedidos);
                      console.log($scope.pedidosArray);
                  }
              }
          );
      };

      $scope.getEstado = function(estadoId) {
          return HelperService.getEstadoPedidoById(estadoId);
      };

      $scope.formatDate = function(dateString) {
          return HelperService.formatDate(dateString);
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if(current.indexOf('alta-pedido') !== -1) {
            $scope.listaId = $routeParams.listaId;
            $scope.getPrecios();
          }

          if(current.indexOf('pedidos-comerciante') !== -1) {
              $scope.userData = SessionService.getUserData();
              $scope.getPedidos();
          }
      };

      $scope.formatDateTime = function(dateString) {
          return HelperService.formatDateTime(dateString);
      };

      $scope.detectMode();
  });
