'use strict';

angular.module('cpApp')
  .controller('ReportesController', function ($scope, $location, StockService, HelperService) {
      $scope.productsArray = [],
      $scope.loading = true;
      $scope.movimientos = [];
      $scope.loading = true;

      $scope.getMovimientos = function() {
          StockService.getMovimientos().$promise.then(
              function (res) {
                  $scope.movimientos = HelperService.transformToArray(res.data.movimientos);
                  $scope.loading = false;
              }
          );
      };

      $scope.detectMode = function() {
          $scope.getMovimientos();
      };

      $scope.detectMode();
  });
