'use strict';

angular.module('cpApp')
  .controller('PedidoController', function ($routeParams, $scope, $location, HelperService, PreciosService, SessionService, ProductService, ProveedorService, PedidosService) {
      $scope.loading = true;
      $scope.preciosArray = [];
      $scope.loading = true;
      $scope.pedido = {};
      $scope.pedidos = [];



      $scope.getEstado = function(estadoId) {
          return HelperService.getEstadoPedidoById(estadoId);
      };

      $scope.formatDate = function(dateString) {
          return HelperService.formatDate(dateString);
      };

      $scope.getPedido = function() {
          PedidosService.getPedidoById($scope.pedidoId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.preciosArray = HelperService.transformToArray(res.data.precios);
                      $scope.pedidoPrecioObj = $scope.processPedidoPrecio(res.data.pedidoPrecio);
                      $scope.pedidos = HelperService.transformToArray(res.data.pedidoPrecio);
                      $scope.pedido = $scope.pedidos[0].Pedido;
                  }
              }
          );
      };

      $scope.processPedidoPrecio = function(pedidosPrecios) {
          var arrayPedidosPrecios = HelperService.transformToArray(pedidosPrecios),
              resultObj = {};

          for (var i = 0; i < arrayPedidosPrecios.length; i++) {
              var p = arrayPedidosPrecios[i];
              resultObj[p.PrecioId] = p.Cantidad;
          }

          return resultObj;
      };

      $scope.detectMode = function() {
          $scope.pedidoId = $routeParams.pedidoId;
          $scope.getPedido();
      };

      $scope.formatDateTime = function(dateString) {
          return HelperService.formatDateTime(dateString);
      };

      $scope.detectMode();
  });
