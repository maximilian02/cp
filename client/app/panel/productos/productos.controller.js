'use strict';

angular.module('cpApp')
  .controller('ProductosController', function ($scope, $route, $location, ProductService) {
      $scope.actionSuccess = false;
      $scope.actionError = false;
      $scope.productsList = [];

      $scope.createProduct = function (data) {
          $scope.actionSuccess = true;
          ProductService.createProduct(data).$promise.then(
            function (res) {
              if(res.data.response) {
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };

      $scope.reloadPage = function () {
          $route.reload();
      };

      $scope.detectMode = function () {
          var current = $location.path();
          if(current.indexOf('productos') !== -1) {
              ProductService.getProducts().$promise.then(
                  function (res) {
                      $scope.productsList = res.data.products;
                      console.log(res.data.products);
                  }
              );
          }
      };

      $scope.detectMode();
  });
