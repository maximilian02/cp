'use strict';

angular.module('cpApp')
  .controller('PanelController', function ($scope, $location, SessionService, UserService) {
      $scope.userData = SessionService.getUserData();
      var currentUserType = $scope.userData.userType;
      $scope.panelSectionList = UserService.getPanelDataByUserTypeId(currentUserType);
      console.log($scope.userData.entityData);

      $scope.validateEntity = function(needValidation) {
          if(needValidation) {
              return $scope.userData.entityData ? true : false;
          } else {
              return true;
          }
      };
  });
