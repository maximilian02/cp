'use strict';

angular.module('cpApp')
  .controller('ListasController', function ($rootScope, $routeParams, $scope, $location, HelperService, PreciosService, SessionService, ProductService) {
      $scope.actionSuccess = false;
      $scope.actionError = false;
      $scope.actionPrecioSuccess = false;
      $scope.actionPrecioError = false;
      $scope.userData = SessionService.getUserData();
      $scope.userType = $scope.userData.userType;
      $scope.hasEntity = $scope.userData.entityData ? true : false;
      $scope.dataValues = {};
      $scope.dataListaValues = {};
      $scope.hasChange = false;
      $scope.loading = true;
      $scope.listasArray = [];
      $scope.preciosArray = [];
      $scope.listaId = 0;
      $scope.productSelected = false;
      $scope.productObjSelected = {};

      $scope.createLista = function (data) {
          $scope.actionSuccess = true;
          PreciosService.createLista(data).$promise.then(
            function (res) {
              if(res.data.response) {
                  $scope.listaId = res.data.listaId;
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };


      $scope.updateLista = function (data) {
          $scope.actionSuccess = true;
          PreciosService.updateLista($routeParams.listaId, data).$promise.then(
            function (res) {
              if(res.data.response) {
                  console.log(res.data.response);
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };

      $scope.deleteLista = function (listaId) {
          var confirmar = confirm('Está seguro que desea eliminar la siguiente lista de precios? Tenga en cuenta que todos los precios relacionados tambíen serán eliminados.');
          if(confirmar) {
              $scope.actionSuccess = true;
              PreciosService.deleteLista(listaId).$promise.then(
                function (res) {
                  if(res.data.response) {
                      $scope.getListas();
                      $scope.actionSuccess = true;
                      $scope.actionError = false;
                  } else {
                      $scope.actionSuccess = false;
                      $scope.actionError = true;
                  }
                }
              );
          }
      };

      $scope.getProducts = function() {
          ProductService.getProducts().$promise.then(
              function (res) {
                  $scope.productsArray = res.data.products;
              }
          );
      };

      $scope.selectProduct = function(product) {
          $scope.productSelected = true;
          $scope.productObjSelected = product;
      };


      $scope.getListas = function () {
          PreciosService.getListas().$promise.then(
            function (res) {
              $scope.loading = false;
              if(res.data.response) {
                  $scope.listasArray = HelperService.transformToArray(res.data.listas);
                  console.log($scope.listasArray);
              }
            }
          );
      };

      $scope.getLista = function (listaId) {
          PreciosService.getLista(listaId).$promise.then(
            function (res) {
              $scope.loading = false;
              if(res.data.response) {
                  $scope.lista = HelperService.transformToArray(res.data.listas)[0];
                  $scope.dataListaValues.listaName = $scope.lista.ListaPrecios.Nombre;
                  $scope.dataListaValues.listaVencimiento = new Date($scope.lista.ListaPrecios.FechaVencimiento);
                  console.log($scope.lista);
              }
            }
          );
      };

      $scope.createPrecio = function(data){
          delete data.productName;
          data.productId = $scope.productObjSelected.Id;
          data.listaId = $routeParams.listaId;

          PreciosService.createPrecio(data).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.getPrecios();
                      $scope.actionPrecioSuccess = true;
                      $scope.actionPrecioError = false;
                      $scope.deleteSelected();
                  } else {
                      $scope.actionPrecioSuccess = false;
                      $scope.actionPrecioError = true;
                  }
                  $scope.actionPrecioMessage = res.data.message;
              }
          );
      };

      $scope.deleteSelected = function () {
          $scope.productSelected = false;
          $scope.productObjSelected = {};
          $scope.dataValues.productName = '';

          if($scope.dataValues.precio) {
            $scope.dataValues.precio = '';
          }
      };

      $scope.getPrecios = function() {
          PreciosService.getPreciosByListaId($routeParams.listaId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.preciosArray = HelperService.transformToArray(res.data.precios);
                  }
              }
          );
      };

      $scope.deletePrecio = function(precioId) {
          var confirmar = confirm('Está seguro que desea eliminar el precio?');
          if(confirmar) {
              PreciosService.deletePrecio(precioId).$promise.then(
                function (res) {
                  if(res.data.response) {
                      $scope.getPrecios();
                      $scope.actionPrecioSuccess = true;
                      $scope.actionPrecioError = false;
                  } else {
                      $scope.actionPrecioSuccess = false;
                      $scope.actionPrecioError = true;
                  }
                  $scope.actionPrecioMessage = res.data.message;
                }
              );
          }
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if($scope.userType !== 2) {
              $location.path('/');
          }

          if(current.indexOf('listas') !== -1) {
              $scope.getListas();
          }

          if(current.indexOf('modificar') !== -1) {
              if(!$scope.userData.entityData) {
                  $location.path('/');
              }

              if($scope.userType === 2) {
                  $scope.listaId = $routeParams.listaId
                  $scope.getLista($routeParams.listaId);
                  $scope.getProducts();
                  $scope.getPrecios();
              }
          } else {
              if(!$scope.userData.entityData) {
                  $location.path('/');
              }
          }
      };

      $scope.formatDate = function(dateString) {
          return HelperService.formatDate(dateString);
      }

      // listen for the refreshUserData event
      $rootScope.$on('refreshUserData', function (event, data) {
          $scope.userData = SessionService.getUserData();
      });

      $scope.detectMode();
  });
