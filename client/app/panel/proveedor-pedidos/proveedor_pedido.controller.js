'use strict';

angular.module('cpApp')
  .controller('ProveedorPedidoController', function ($routeParams, $scope, $location, HelperService, PreciosService, SessionService, ProductService, ProveedorService, PedidosService) {
      $scope.loading = true;
      $scope.preciosArray = [];
      $scope.pedidosArray = [];
      $scope.listaPreciosData = {};
      $scope.proveedorData = {};
      $scope.dataListaValues = {};
      $scope.actionSuccess = false;
      $scope.actionError = false;
      $scope.loading = true;
      $scope.pedidoActual = {
          preciosObj: {},
          preciosArray: [],
          pedidoComment: ''
      };

      $scope.getPrecios = function() {
          PreciosService.getPreciosByListaId($routeParams.listaId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.listaPreciosData = HelperService.transformToArray(res.data.listaPrecios)[0];
                      $scope.loading = false;
                      $scope.preciosArray = HelperService.transformToArray(res.data.precios);
                      $scope.proveedorData.id = HelperService.transformToArray($scope.listaPreciosData.RProveedorListaprecioss)[0].ProveedorId;
                      $scope.getProveedor($scope.proveedorData.id);
                  }
              }
          );
      };

      $scope.getProveedor = function(proveedorId) {
          ProveedorService.getProveedor(proveedorId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.proveedorData = HelperService.transformToArray(res.data.proveedor)[0];
                      // console.log($scope.proveedorData);
                  }
              }
          );
      };

      $scope.getPedidos = function() {
          PedidosService.getPedidosByProveedorId($scope.userData.entityData.id).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.pedidosArray = HelperService.transformToArray(res.data.pedidos);
                      console.log($scope.pedidosArray);
                  }
              }
          );
      };

      $scope.getEstado = function(estadoId) {
          return HelperService.getEstadoPedidoById(estadoId);
      };

      $scope.formatDate = function(dateString) {
          return HelperService.formatDate(dateString);
      };

      $scope.detectMode = function() {
          var current = $location.path();

          $scope.userData = SessionService.getUserData();
          $scope.getPedidos();
      };

      $scope.formatDateTime = function(dateString) {
          return HelperService.formatDateTime(dateString);
      };

      $scope.detectMode();
  });
