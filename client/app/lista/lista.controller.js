'use strict';

angular.module('cpApp')
  .controller('ListaController', function ($routeParams, $scope, $location, HelperService, PreciosService, SessionService, ProductService, ProveedorService) {
      $scope.loading = true;
      $scope.preciosArray = [];
      $scope.listaPreciosData = {};
      $scope.proveedorData = {};
      $scope.userData = SessionService.getUserData();

      $scope.getPrecios = function() {
          PreciosService.getPreciosByListaId($routeParams.listaId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.listaPreciosData = HelperService.transformToArray(res.data.listaPrecios)[0];
                      $scope.loading = false;
                      $scope.preciosArray = HelperService.transformToArray(res.data.precios);
                      $scope.proveedorData.id = HelperService.transformToArray($scope.listaPreciosData.RProveedorListaprecioss)[0].ProveedorId;
                      $scope.getProveedor($scope.proveedorData.id);
                  }
              }
          );
      };

      $scope.getProveedor = function(proveedorId) {
          ProveedorService.getProveedor(proveedorId).$promise.then(
              function (res) {
                  if(res.data.response) {
                      $scope.proveedorData = HelperService.transformToArray(res.data.proveedor)[0];
                      console.log($scope.proveedorData);
                  }
              }
          );
      };

      $scope.getPreferenciaPago = function(preferenciaId) {
          return HelperService.getPreferenciaPagoById(preferenciaId);
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if(current.indexOf('lista') !== -1) {
            $scope.listaId = $routeParams.listaId
            $scope.getPrecios();
          }
      };

      $scope.formatDate = function(dateString) {
          return HelperService.formatDate(dateString);
      }

      $scope.detectMode();
  });
