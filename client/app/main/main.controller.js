'use strict';

angular.module('cpApp')
    .controller('MainController', function ($scope, PreciosService, HelperService) {
        $scope.listasArray = [];
        $scope.getListas = function () {
            PreciosService.getListas(true).$promise.then(
              function (res) {
                $scope.loading = false;
                if(res.data.response) {
                    $scope.listasArray = HelperService.transformToArray(res.data.listas);
                }
              }
            );
        };

        $scope.formatDate = function(dateString) {
            return HelperService.formatDate(dateString);
        }

        $scope.getListas();
    });
