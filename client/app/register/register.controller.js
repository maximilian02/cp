'use strict';

angular.module('cpApp')
  .controller('RegisterController', function ($scope, APIService, SessionService) {
      $scope.registerError = false;
      $scope.registerSuccess = false;
      $scope.responseMessage = '';
      $scope.hasChange = false;

      $scope.register = function (data) {
          APIService.createData('users', data)
              .$promise.then(
                  function (res) {
                      if(res) {
                          if(res.data.response) {
                              $scope.registerSuccess = true;
                              $scope.registerError = false;
                          } else {
                              $scope.registerError = true;
                              $scope.responseMessage = res.data.message;
                          }
                      }
                      data.email = '';
                  }
              );
      };

      SessionService.redirectIfLogged('/');
  });
