'use strict';

angular.module('cpApp')
  .controller('LoginController', function ($scope, $location, APIService, SessionService, $window) {
      $scope.successLogin = false;
      $scope.responseMessage = '';

      $scope.login = function (data) {
          APIService.login(data)
              .$promise.then(
                  function (res) {
                      if(res) {
                          // console.log('Login Response', res.data);
                          if(res.data.response) {
                              SessionService.executeLogIn(res.data.userData);
                              $scope.successLogin = true;
                              $scope.errorLogin = false;

                              $window.location.reload();
                          } else {
                              $scope.errorLogin = true;
                          }
                          $scope.responseMessage = res.data.message;
                      }
                      data.password = '';
                  }
              );
      };

      SessionService.redirectIfLogged('/');
  });
