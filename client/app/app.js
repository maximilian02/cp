'use strict';

//App settings
window.NG_APP_ENV = window.NG_APP_ENV || {};

angular.module('cpApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap'
])
  .config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {
      $routeProvider
          .when('/register', {
              templateUrl: 'app/register/register.html',
              controller: 'RegisterController'
          })
          .when('/login', {
              templateUrl: 'app/login/login.html',
              controller: 'LoginController'
          })
          .when('/perfil', {
              templateUrl: 'app/perfil/perfil.html',
              controller: 'PerfilController',
              resolve:{
                "check":function(SessionService){
                    SessionService.redirectIfNotLogged('/');
                }
              }
          })
              .when('/alta-comercio', {
                  templateUrl: 'app/comercio/alta_comercio.html',
                  controller: 'ComercioController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.verifyType(1);
                    }
                  }
              })
              .when('/modificar-comercio', {
                  templateUrl: 'app/comercio/modificar_comercio.html',
                  controller: 'ComercioController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.verifyType(1);
                        SessionService.verifyEntity();
                    }
                  }
              })
              .when('/alta-proveedor', {
                  templateUrl: 'app/proveedor/alta_proveedor.html',
                  controller: 'ProveedoresController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.verifyType(2);
                    }
                  }
              })
              .when('/modificar-proveedor', {
                  templateUrl: 'app/proveedor/modificar_proveedor.html',
                  controller: 'ProveedoresController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.verifyType(2);
                        SessionService.verifyEntity();
                    }
                  }
              })
          .when('/panel', {
              templateUrl: 'app/panel/panel.html',
              controller: 'PanelController',
              resolve:{
            		"check":function(SessionService){
                    SessionService.redirectIfNotLogged('/');
            		}
            	}
          })
          // Routes related to panel
              .when('/productos', {
                  templateUrl: 'app/panel/productos/listado_productos.html',
                  controller: 'ProductosController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.redirectIfNotLogged('/');
                    }
                  }
              })
                  .when('/alta-producto', {
                      templateUrl: 'app/panel/productos/alta_productos.html',
                      controller: 'ProductosController',
                      resolve:{
                    		"check":function(SessionService){
                            SessionService.redirectIfNotLogged('/');
                    		}
                    	}
                  })
              .when('/pedido/:pedidoId', {
                  templateUrl: 'app/panel/pedido/pedido.html',
                  controller: 'PedidoController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.redirectIfNotLogged('/');
                    }
                  }
              })
              .when('/pedidos-proveedor', {
                  templateUrl: 'app/panel/proveedor-pedidos/pedidos_proveedores.html',
                  controller: 'ProveedorPedidoController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.verifyType(2);
                    }
                  }
              })
              .when('/pedidos-comerciante', {
                  templateUrl: 'app/panel/comercio-pedidos/pedidos_comercios.html',
                  controller: 'ComercioPedidoController',
                  resolve:{
                    "check":function(SessionService){
                        SessionService.verifyType(1);
                    }
                  }
              })
                  .when('/alta-pedido/:listaId', {
                      templateUrl: 'app/panel/comercio-pedidos/alta_pedido.html',
                      controller: 'ComercioPedidoController',
                      resolve:{
                    		"check":function(SessionService){
                            SessionService.verifyType(1);
                    		}
                    	}
                  })
              .when('/stock', {
                  templateUrl: 'app/panel/stock/stock.html',
                  controller: 'StockController',
                  resolve:{
                		"check":function(SessionService){
                        SessionService.redirectIfNotLogged('/');
                		}
                	}
              })
                  .when('/egreso', {
                      templateUrl: 'app/panel/stock/egreso/egreso.html',
                      controller: 'StockController',
                      resolve:{
                        "check":function(SessionService){
                            SessionService.redirectIfNotLogged('/');
                        }
                      }
                  })
                  .when('/ingreso', {
                      templateUrl: 'app/panel/stock/ingreso/ingreso.html',
                      controller: 'StockController',
                      resolve:{
                        "check":function(SessionService){
                            SessionService.redirectIfNotLogged('/');
                        }
                      }
                  })
              .when('/listas', {
                  templateUrl: 'app/panel/listas/listas.html',
                  controller: 'ListasController',
                  resolve:{
                		"check":function(SessionService){
                        SessionService.redirectIfNotLogged('/');
                		}
                	}
              })
                  .when('/alta-lista', {
                      templateUrl: 'app/panel/listas/alta-lista.html',
                      controller: 'ListasController',
                      resolve:{
                        "check":function(SessionService){
                            SessionService.redirectIfNotLogged('/');
                        }
                      }
                  })
                  .when('/modificar-lista/:listaId', {
                      templateUrl: 'app/panel/listas/modificar-lista.html',
                      controller: 'ListasController',
                      resolve:{
                        "check":function(SessionService){
                            SessionService.verifyType(2);
                            SessionService.redirectIfNotLogged('/');
                        }
                      }
                  })
              .when('/reportes', {
                  templateUrl: 'app/panel/reportes/reporte.html',
                  controller: 'ReportesController',
                  resolve:{
                		"check":function(SessionService){
                        SessionService.redirectIfNotLogged('/');
                		}
                	}
              })

          .when('/lista/:listaId', {
              templateUrl: 'app/lista/lista.html',
              controller: 'ListaController',
          })
          .when('/proveedor/:proveedorId', {
              templateUrl: 'app/proveedor/proveedor.html',
              controller: 'ProveedorController',
          })
          .when('/logout', {
              templateUrl: 'app/logout/logout.html'
          })
          // Home route
          .when('/', {
              templateUrl: 'app/main/main.html',
              controller: 'MainController'
          })
          .otherwise({
              redirectTo: '/'
          });

      $locationProvider.html5Mode(true);
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
  }]);
  // .run(function ($rootScope, $location) {
  //     // Register listener to watch route changes.
  //     $rootScope.$on('$routeChangeStart', function (event, next, current) {
  //         // DO I NEED TO CHECK SOMETHING EVERYTIME WE NAVIGATE TO A NEW ROUTE?
  //         // I NEED TO THINK ABOUT IT
  //         //console.log('sdfsdf', SessionService.isLogged());
	// 	      //if (!AuthenticationModel.isSignedIn() && next.requireAuthentication === true) {
  //           //  $location.path('/signin');
  //         //}
	//      });
  //
  // });
