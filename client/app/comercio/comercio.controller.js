'use strict';

angular.module('cpApp')
  .controller('ComercioController', function ($rootScope, $scope, $location, SessionService, ComercioService, UserService, HelperService, $window) {
      $scope.actionSuccess = false;
      $scope.actionError = false;
      $scope.userData = SessionService.getUserData();
      $scope.userType = $scope.userData.userType;
      $scope.cuitCuilHasChange = false;
      $scope.telefonoHasChange = false;
      $scope.dataValues = {};

      $scope.createComercio = function (data) {
          $scope.actionSuccess = true;
          ComercioService.createComercio(data).$promise.then(
            function (res) {
              if(res.data.response) {
                  UserService.updateUserData($scope.userData.id);
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };

      $scope.updateComercio = function (data) {
          $scope.actionSuccess = true;
          ComercioService.updateComercio(data).$promise.then(
            function (res) {
              if(res.data.response) {
                  console.log(res.data.response);
                  UserService.updateUserData($scope.userData.id);
                  $scope.actionSuccess = true;
                  $scope.actionError = false;
              } else {
                  $scope.actionSuccess = false;
                  $scope.actionError = true;
              }
            }
          );
      };

      $scope.validarCuitCuil = function (cuitCuil) {
          return HelperService.validarCuit(cuitCuil);
      };

      $scope.validarTelefono = function (telefono) {
          return HelperService.validarTelefono(telefono);
      };

      $scope.detectMode = function() {
          var current = $location.path();

          if($scope.userType !== 1) {
              $location.path('/');
          }

          if(current.indexOf('modificar') !== -1) {
              if(!$scope.userData.entityData) {
                  $location.path('/');
              }

              if($scope.userType === 1) {
                  $scope.dataValues = {
                      comercioName: $scope.userData.entityData.nombre,
                      comercioEmail: $scope.userData.entityData.email,
                      comercioRazonSocial: $scope.userData.entityData.razonSocial,
                      comercioCuitCuil: $scope.userData.entityData.cuitCuil,
                      comercioDireccion: $scope.userData.entityData.direccion,
                      comercioTelefono: $scope.userData.entityData.telefono
                  };
              }
          } else {
              if($scope.userData.entityData) {
                  $location.path('/');
              }
          }
      };

      $scope.detectMode();

      // listen for the refreshUserData event
      $rootScope.$on('refreshUserData', function (event, data) {
          $scope.userData = SessionService.getUserData();
      });
  });
